package com.somee.rothnaksomang.homework3_ui_and_event;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

public class DispayInformation extends AppCompatActivity {

    private TextView tvTitle,tvUrl,tvEmail,tvTypeUrl,tvCountryName_Display;
    private Spinner spnCountry;
    private Uri imageUrl;
    private ImageView ivProfile;
    private Button btnBack;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_information);

        tvTitle=findViewById(R.id.tvTitle);
        tvEmail=findViewById(R.id.tvEmail);
        tvUrl=findViewById(R.id.tvWebsite);
        tvTypeUrl=findViewById(R.id.tvUrlType);
        btnBack=findViewById(R.id.btnBack);
        ivProfile=findViewById(R.id.ivProfile_display);
        tvCountryName_Display=findViewById(R.id.tvCountryName_Display);


        Intent i=getIntent();
        WebsiteInfo websiteInfo=i.getParcelableExtra("websiteInformation");

        tvTitle.setText(websiteInfo.getTitle());
        tvTypeUrl.setText(websiteInfo.getUrlType());
        tvUrl.setText(websiteInfo.getUrlWebsite());
        tvEmail.setText(websiteInfo.getEmail());
        tvCountryName_Display.setText(websiteInfo.getCountrylist().getName());

        imageUrl=Uri.parse(websiteInfo.getImageUrl());
        ivProfile.setImageURI(imageUrl);

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(getApplicationContext(),MainActivity.class);
                startActivity(intent);
            }
        });





    }
}
