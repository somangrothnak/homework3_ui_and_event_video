package com.somee.rothnaksomang.homework3_ui_and_event;

import android.os.Parcel;
import android.os.Parcelable;

public class WebsiteInfo implements Parcelable {
    private String title;
    private String urlType;
    private String urlWebsite;
    private String email;
    private ItemData countrylist;
    private String imageUrl;

    public WebsiteInfo(String title, String urlType, String urlWebsite, String email, ItemData countrylist, String imageUrl) {
        this.title = title;
        this.urlType = urlType;
        this.urlWebsite = urlWebsite;
        this.email = email;
        this.countrylist = countrylist;
        this.imageUrl = imageUrl;
    }

    protected WebsiteInfo(Parcel in) {
        title = in.readString();
        urlType = in.readString();
        urlWebsite = in.readString();
        email = in.readString();
        countrylist = in.readParcelable(ItemData.class.getClassLoader());
        imageUrl = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(title);
        dest.writeString(urlType);
        dest.writeString(urlWebsite);
        dest.writeString(email);
        dest.writeParcelable(countrylist, flags);
        dest.writeString(imageUrl);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<WebsiteInfo> CREATOR = new Creator<WebsiteInfo>() {
        @Override
        public WebsiteInfo createFromParcel(Parcel in) {
            return new WebsiteInfo(in);
        }

        @Override
        public WebsiteInfo[] newArray(int size) {
            return new WebsiteInfo[size];
        }
    };

    public String getTitle() {
        return title;
    }

    public String getUrlType() {
        return urlType;
    }

    public String getUrlWebsite() {
        return urlWebsite;
    }

    public String getEmail() {
        return email;
    }

    public ItemData getCountrylist() {
        return countrylist;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    @Override
    public String toString() {
        return "WebsiteInfo{" +
                "title='" + title + '\'' +
                ", urlType='" + urlType + '\'' +
                ", urlWebsite='" + urlWebsite + '\'' +
                ", email='" + email + '\'' +
                ", countrylist=" + countrylist +
                ", imageUrl='" + imageUrl + '\'' +
                '}';
    }
}
