package com.somee.rothnaksomang.homework3_ui_and_event;

import android.os.Parcel;
import android.os.Parcelable;

public class ItemData implements Parcelable {
    private String name;
    private Integer icon;

    public ItemData(String name, Integer icon) {
        this.name = name;
        this.icon = icon;
    }

    protected ItemData(Parcel in) {
        name = in.readString();
        if (in.readByte() == 0) {
            icon = null;
        } else {
            icon = in.readInt();
        }
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        if (icon == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(icon);
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ItemData> CREATOR = new Creator<ItemData>() {
        @Override
        public ItemData createFromParcel(Parcel in) {
            return new ItemData(in);
        }

        @Override
        public ItemData[] newArray(int size) {
            return new ItemData[size];
        }
    };

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getIcon() {
        return icon;
    }

    public void setIcon(Integer icon) {
        this.icon = icon;
    }
}
