package com.somee.rothnaksomang.homework3_ui_and_event;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class SpinnerAdapter extends ArrayAdapter<ItemData> {
    private Integer groupId;
    private Activity context;
    ArrayList<ItemData> list;
    LayoutInflater inflater;

    public SpinnerAdapter(Activity context, Integer groupId, int id, ArrayList<ItemData> list) {
        super(context, groupId, id, (List<ItemData>) list);
        this.list=list;
        inflater=(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.groupId=groupId;
    }

    public View getView(int position, View convertView, ViewGroup parent){
        View itemView=inflater.inflate(groupId,parent,false);
        ItemData itemData;

        itemData=list.get(position);

        ImageView imageView=itemView.findViewById(R.id.ivFlagCountry);
        TextView textView=itemView.findViewById(R.id.tvCountryName);

        imageView.setImageResource(itemData.getIcon());
        textView.setText(itemData.getName());
        return itemView;
    }

    public View getDropDownView(int position,View convertView,ViewGroup parent){
        return getView(position,convertView,parent);
    }


}
