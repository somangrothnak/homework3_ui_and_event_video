package com.somee.rothnaksomang.homework3_ui_and_event;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private EditText etTitle,etUrl,etEmail;
    private RadioButton rdbWebsite,rdbFacebook;
    private Spinner spnCountry;
    private String rdbTypeUrl;
    private Uri imageUrl;
    private ImageView ivProfile;
    private Button btnSave,btnChooseImage;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
//        set value to Spinner
        initValueSpinner();
//        set referent variable
        setReferentVariable();
//        set default value to view
        initValueView();

        btnChooseImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ChooseImageFromGallery();
            }
        });

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getSaveData();
            }
        });


    }
    public void ChooseImageFromGallery(){
        Intent intentGallery=new Intent(Intent.ACTION_PICK,MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intentGallery,1);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(resultCode==this.RESULT_CANCELED){
            return;
        }
        if(requestCode==1){
            if(data!=null){
               imageUrl=data.getData();
                try {
                    Bitmap bitmap=MediaStore.Images.Media.getBitmap(this.getContentResolver(),imageUrl);
                    ivProfile.setImageBitmap(bitmap);
                    Toast.makeText(getApplicationContext(),"Imgae is selected",Toast.LENGTH_SHORT).show();
                } catch (IOException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(),"choose image failed",Toast.LENGTH_SHORT).show();
                }
            }
        }

    }

    public void getSaveData(){

        ItemData countryList= (ItemData) spnCountry.getSelectedItem();

        if(rdbWebsite.isChecked()==true){
            rdbTypeUrl=rdbWebsite.getText().toString();
        }else if(rdbFacebook.isChecked()==true){
            rdbTypeUrl=rdbFacebook.getText().toString();
        }

        if(etTitle.getText().toString().equals("")){
            etTitle.requestFocus();
            Toast.makeText(getApplicationContext(),"Title NUll",Toast.LENGTH_SHORT).show();
        }else if(etUrl.getText().toString().equals("")){
            etUrl.requestFocus();
            Toast.makeText(getApplicationContext(),"URL NUll",Toast.LENGTH_SHORT).show();
        }else if(etEmail.getText().toString().equals("")){
            etEmail.requestFocus();
            Toast.makeText(getApplicationContext(),"Email NUll",Toast.LENGTH_SHORT).show();
        }else if(imageUrl==null){
            Toast.makeText(getApplicationContext(),"Image NUll",Toast.LENGTH_SHORT).show();
        }else{
            WebsiteInfo websiteInfo =new WebsiteInfo(etTitle.getText().toString(),rdbTypeUrl,etUrl.getText().toString(),etEmail.getText().toString(),countryList,imageUrl.toString());
            Intent i=new Intent(getApplicationContext(),DispayInformation.class);
            i.putExtra("websiteInformation",websiteInfo);
            startActivity(i);
        }
    }

    public void setReferentVariable(){
        etTitle=findViewById(R.id.etTitle);
        etUrl=findViewById(R.id.etWebsite);
        etEmail=findViewById(R.id.etEmail);
        rdbFacebook=findViewById(R.id.rdbFacebook);
        rdbWebsite=findViewById(R.id.rdbWebsite);
        ivProfile=findViewById(R.id.ivProfile);
        btnSave=findViewById(R.id.btnSave);
        btnChooseImage=findViewById(R.id.btnChooseImage);
        spnCountry=findViewById(R.id.spnCountry);
    }

    public void initValueView(){
        rdbWebsite.setChecked(true);
    }

    public void initValueSpinner(){
        ArrayList<ItemData> list=new ArrayList<>();

        list.add(new ItemData("Cambodia",R.drawable.ic_cambodia));
        list.add(new ItemData("Japan",R.drawable.ic_japan));
        list.add(new ItemData("South Korea",R.drawable.ic_south_korea));

        Spinner spinner=findViewById(R.id.spnCountry);

        SpinnerAdapter spinnerAdapter=new SpinnerAdapter(this,R.layout.activity_spinner_country,R.id.tvCountryName,list);

        spinner.setAdapter(spinnerAdapter);


    }
}
